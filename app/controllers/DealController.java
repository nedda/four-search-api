package controllers;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import play.Logger;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import uk.co.wowcher.wow.plugin.es.ES;
import uk.co.wowcher.wow.plugin.es.ElasticDocument;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

public class DealController extends Controller {

    @Inject
    public DealController(ES es) {
        ElasticDocument.registerAsType(es);
    }


    /**
     * Normal call should return

     {
     mainDeal: deal,
     sideDeals: [deal, deal, deal],
     deals: [deal, deal, deal]
     }

     Where Deal is a full deal object


     ?sidedeals=false should return
     {
     mainDeal: deal,
     deals: [deal, deal, deal]
     }
     * @param location
     * @return
     */
    public F.Promise<Result> dealsByLocation(String location) {
        Date today = Calendar.getInstance().getTime();
       // Get all deals for this location
        QueryBuilder dealsByLocation = QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("wowcherLocations", location))
               // .must(QueryBuilders.termQuery("site", "livingsocial"))
                // get only deals that are valid now
                .must(QueryBuilders.rangeQuery("startDate").lte(today.getTime()))
                .must(QueryBuilders.rangeQuery("closingDate").gt(today.getTime()));

        return new ElasticDocument("deal").search(s->s.setQuery(dealsByLocation), dealsByLocation, 100, "deal").map(jsonResponse -> {
                Logger.info("Got results from ES Search");
            // TODO Build up the list of deals to return in the specified format - main, side, deals
                return ok(jsonResponse);
        });
    }

    public F.Promise<Result> getAll(String dataType) {
        return new ElasticDocument(dataType).findAll(dataType).map(res-> ok(res));
    }
}