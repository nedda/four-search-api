package controllers;

import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import uk.co.wowcher.wow.plugin.es.ES;
import uk.co.wowcher.wow.plugin.es.ElasticDocument;

import javax.inject.Inject;

public class ElasticsearchDocumentController extends Controller {

    @Inject
    public ElasticsearchDocumentController(ES es) {
        ElasticDocument.registerAsType(es);
    }

    public F.Promise<Result> getById(Integer id, String dataType) {
        return ElasticDocument.get(dataType, id.toString()).map(jsonResponse -> {
            if (jsonResponse.isPresent()) return ok(jsonResponse.get());
            else return notFound("Did not find item with id " + id);
        });
    }

    public F.Promise<Result> getAll(String dataType) {
        return new ElasticDocument(dataType).findAll(dataType).map(res-> ok(res));
    }

}